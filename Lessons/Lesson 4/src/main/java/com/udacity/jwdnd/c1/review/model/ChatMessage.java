package com.udacity.jwdnd.c1.review.model;

import java.util.ArrayList;
import java.util.List;

public class ChatMessage {
    private List<ChatMessage> chatMessage;
    private String username;
    private String messageText;

    public void postConstruct() {
        this.chatMessage = new ArrayList<>();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public void addMessage(ChatMessage chatMessage){
        this.chatMessage.add(chatMessage);
    }

}
